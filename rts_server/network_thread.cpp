#include "network_thread.h"

#include "udpsocket.h"
#include "lfqueuewithnotification.h"

#include <poll.h>
#include <iostream>
#include <unordered_map>


static void routine (NotificationFD terminate_fd, NotificationFD finished_fd,
                     const std::string& host, uint16_t port,
                     std::shared_ptr<LFQueueWithNotification<std::shared_ptr<HCCN::ClientToServer::Message>>> input_queue_p,
                     std::shared_ptr<LFQueueWithNotification<std::shared_ptr<HCCN::ServerToClient::Message>>> output_queue_p,
                     std::shared_ptr<std::string> error_message)
{
    finished_fd.EnableNotifictionOnDestruction ();

    LFQueueWithNotification<std::shared_ptr<HCCN::ClientToServer::Message>>& input_queue = *input_queue_p;
    LFQueueWithNotification<std::shared_ptr<HCCN::ServerToClient::Message>>& output_queue = *output_queue_p;

    std::string error_message_str;
    std::unique_ptr<UDPSocket> socket = UDPSocket::Create (host, port, error_message_str);
    if (!socket) {
        error_message = std::make_shared<std::string> ("Failed to create UDP socket: " + error_message_str);
        return;
    }

    std::unordered_map<HCCN::TransportMessageIdentifier, std::shared_ptr<HCCN::ClientToServer::MessageFragmentCollector>, HCCN::TransportMessageIdentifierHash> input_fragment_queue;

    while (true) {
        int output_queue_event_fd = output_queue.GetEventFD ();
        struct pollfd pfds[3] = {
            {
                .fd = terminate_fd.FD (),
                .events = POLLIN,
                .revents = 0,
            },
            {
                .fd = socket->FD (),
                .events = POLLIN,
                .revents = 0,
            },
            {
                .fd = output_queue_event_fd,
                .events = POLLIN,
                .revents = 0,
            },
        };
        int ret = poll (pfds, sizeof (pfds)/sizeof (pfds[0]), -1);
        if (ret < 0) {
            error_message = std::make_shared<std::string> ("Polling failed: " + std::string (strerror (errno)));
            break;
        }
        if (ret > 0) {
            if (pfds[0].revents & POLLIN) {
                break;
            }
            if (pfds[1].revents & POLLIN) {
                std::optional<std::string> socket_error_message;
                for (std::optional<HCCN::Datagram> datagram = socket->GetDatagram (socket_error_message); datagram.has_value (); datagram = socket->GetDatagram (socket_error_message)) {
                    if (std::shared_ptr<HCCN::ClientToServer::MessageFragment> message_fragment = HCCN::ClientToServer::MessageFragment::parse (*datagram)) {
                        HCCN::TransportMessageIdentifier transport_message_identifier (message_fragment->endpoint, message_fragment->request_id);
                        std::unordered_map<HCCN::TransportMessageIdentifier, std::shared_ptr<HCCN::ClientToServer::MessageFragmentCollector>,
                                           HCCN::TransportMessageIdentifierHash>::iterator fragment_collector_it =
                            input_fragment_queue.find (transport_message_identifier);
                        if (fragment_collector_it != input_fragment_queue.end ()) {
                            if (fragment_collector_it->second) {
                                HCCN::ClientToServer::MessageFragmentCollector& fragment_collector = *fragment_collector_it->second;
                                fragment_collector.insert (message_fragment);
                                if (fragment_collector.complete ()) {
                                    input_queue.Add (fragment_collector.build ());
                                    fragment_collector_it->second.reset ();
                                }
                            }
                        } else {
                            std::shared_ptr<HCCN::ClientToServer::MessageFragmentCollector> fragment_collector (new HCCN::ClientToServer::MessageFragmentCollector);
                            fragment_collector->insert (message_fragment);
                            if (fragment_collector->complete ()) {
                                input_queue.Add (fragment_collector->build ());
                                fragment_collector.reset ();
                            }
                            input_fragment_queue.emplace (transport_message_identifier, fragment_collector);
                        }
                    }

                }
            }
            if (pfds[2].revents & POLLIN) {
                std::string error_message_str;
                LFQueueWithNotification<std::shared_ptr<HCCN::ServerToClient::Message>>::List* message_list = output_queue.TakeAllReversed ();
                while (message_list) {
                    std::vector<HCCN::Datagram> datagrams = message_list->value->encode ();
                    for (const HCCN::Datagram& datagram: datagrams)
                        socket->SendDatagram (datagram, error_message_str);
                    LFQueueWithNotification<std::shared_ptr<HCCN::ServerToClient::Message>>::List* next = message_list->next;
                    delete message_list;
                    message_list = next;
                }
            }
        }
    }
}

NetworkThread::NetworkThread (const std::string& host, uint16_t port)
{
    input_queue = std::make_shared<LFQueueWithNotification<std::shared_ptr<HCCN::ClientToServer::Message>>> ();
    output_queue = std::make_shared<LFQueueWithNotification<std::shared_ptr<HCCN::ServerToClient::Message>>> ();

    thread = std::unique_ptr<std::thread> (new std::thread (routine, terminate_fd, finished_fd, host, port, input_queue, output_queue, error_message));
}
NetworkThread::~NetworkThread ()
{
    terminate_fd.Notify ();
    thread->join ();
}
std::shared_ptr<std::string> NetworkThread::Destroy (std::unique_ptr<NetworkThread>& network_thread)
{
    std::shared_ptr<std::string> error_message = network_thread->error_message;
    network_thread.reset ();
    return error_message;
}

void NetworkThread::sendDatagram (const std::shared_ptr<HCCN::ServerToClient::Message>& datagram)
{
    output_queue->Add (std::shared_ptr<HCCN::ServerToClient::Message> (datagram));
}
