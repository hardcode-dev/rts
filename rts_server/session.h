#pragma once

#include "entities.pb.h"
#include "matchstate.h"

// TODO: Locking sessions
struct Session {
    Session () = delete;
    Session (uint64_t session_id)
        : session_id (session_id)
    {
    }
    Session (const HCCN::Endpoint& client_endpoint, const std::string& login, uint64_t session_id)
        : client_endpoint (client_endpoint)
        , login (login)
        , session_id (session_id)
    {
    }

    HCCN::Endpoint client_endpoint;
    std::string login;
    uint64_t session_id;
    std::optional<uint32_t> current_room = {};
    RTS::Role current_role = RTS::ROLE_UNSPECIFIED;
    std::optional<Unit::Team> current_team = {};
    bool query_room_list_requested = false;
    bool ready = false;
};
