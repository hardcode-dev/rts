#include "application.h"

#include <stdio.h>


int main (int argc, char** argv)
{
    Application app (argc, argv);

    if (!app.init ()) {
        fprintf (stderr, "Failed to initialize application\n");
        return 1;
    }
    app.run ();
    return 0;
}
