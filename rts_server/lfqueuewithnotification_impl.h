#include <sys/eventfd.h>
#include <unistd.h>
#include <system_error>


template<typename Value>
LFQueueWithNotification<Value>::LFQueueWithNotification ()
{
    list = nullptr;
    if ((event_fd = eventfd (0, EFD_NONBLOCK)) < 0)
        throw std::system_error (errno, std::system_category (), "Failed to create event FD");
}
template<typename Value>
LFQueueWithNotification<Value>::~LFQueueWithNotification ()
{
    close (event_fd);
    List* head = list;
    while (head) {
        List* next = head->next;
        delete head;
        head = next;
    }
}

template<typename Value>
void LFQueueWithNotification<Value>::Add (Value&& value)
{
    List* new_list = new List;
    new_list->value = std::move (value);
    new_list->next = __atomic_exchange_n (&list, new_list, __ATOMIC_SEQ_CST);
    eventfd_write (event_fd, 1);
}
template<typename Value>
typename LFQueueWithNotification<Value>::List* LFQueueWithNotification<Value>::TakeAllReversed ()
{
    eventfd_t unused_value;
    eventfd_read (event_fd, &unused_value);
    return __atomic_exchange_n (&list, nullptr, __ATOMIC_SEQ_CST);
}
template<typename Value>
int LFQueueWithNotification<Value>::GetEventFD ()
{
    return event_fd;
}
