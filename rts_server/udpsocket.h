#pragma once

#include "common.h"

#include <string>
#include <memory>
#include <optional>


class UDPSocket
{
public:
    enum class SendStatus {
        Success,
        BusySocket,
        Failure,
    };

public:
    static std::unique_ptr<UDPSocket> Create (const std::string& host, uint16_t port, std::string& error_message);

public:
    UDPSocket () = delete;
    UDPSocket (const UDPSocket&) = delete;
    UDPSocket& operator= (const UDPSocket&) = delete;
    UDPSocket (UDPSocket&&) = default;
    int FD ();
    std::optional<HCCN::Datagram> GetDatagram (std::optional<std::string>& error_message);
    SendStatus SendDatagram (const HCCN::Datagram& datagram, std::string& error_message);

private:
    UDPSocket (const std::string& host, uint16_t port, int fd);

private:
    std::string host;
    uint16_t port;
    int fd;
};
