#pragma once


template<typename Value>
class LFQueue
{
public:
    struct List {
        Value value;
        List* next;
    };

public:
    LFQueue ();
    ~LFQueue ();

    void Add (Value&& value);
    List* TakeAllReversed ();

private:
    List* list;
};

#include "lfqueue_impl.h"
