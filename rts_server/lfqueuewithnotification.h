#pragma once


template<typename Value>
class LFQueueWithNotification
{
public:
    struct List {
        Value value;
        List* next;
    };

public:
    LFQueueWithNotification ();
    ~LFQueueWithNotification ();

    void Add (Value&& value);
    List* TakeAllReversed ();
    int GetEventFD ();

private:
    List* list;
    int event_fd;
};

#include "lfqueuewithnotification_impl.h"
