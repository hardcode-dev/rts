#include "udpsocket.h"

#include "common.h"

#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>


std::unique_ptr<UDPSocket> UDPSocket::Create (const std::string& host, uint16_t port, std::string& error_message)
{
    int fd = socket (AF_INET, SOCK_DGRAM, 0);
    if (fd < 0) {
        error_message = std::string ("Failed to create socket: ") + strerror (errno);
        return nullptr;
    }
    struct addrinfo* addr_info;
    struct addrinfo hints = {};
    hints.ai_flags = AI_NUMERICHOST;
    char service[8];
    sprintf (service, "%d", (int) port);
    int addr_info_err = getaddrinfo (host.c_str (), service, &hints, &addr_info);
    if (addr_info_err) {
        error_message = std::string ("Failed to get address info: ") + gai_strerror (addr_info_err);
        close (fd);
        return nullptr;
    }
    if (bind (fd, addr_info->ai_addr, addr_info->ai_addrlen) < 0) {
        error_message = std::string ("Failed to bind to specified address: ") + strerror (errno);
        freeaddrinfo (addr_info);
        close (fd);
        return nullptr;
    }

    freeaddrinfo (addr_info);

    return std::unique_ptr<UDPSocket> (new UDPSocket (host, port, fd));
}
UDPSocket::UDPSocket (const std::string& host, uint16_t port, int fd)
    : host (host), port (port), fd (fd)
{
}

int UDPSocket::FD ()
{
    return fd;
}
std::optional<HCCN::Datagram> UDPSocket::GetDatagram (std::optional<std::string>& error_message)
{
    std::vector<char> buffer (65536);
    HCCN::Endpoint sender;
    socklen_t sender_len = sizeof (sender);
    ssize_t ret = recvfrom (fd, buffer.data (), buffer.size (), MSG_DONTWAIT, (struct sockaddr*) &sender.address, &sender_len);
    if (ret < 0) {
        if (errno != EAGAIN && errno != EWOULDBLOCK)
            error_message = std::string ("Failed to recieve message: ") + strerror (errno);
        return std::nullopt;
    }
    buffer.resize (ret);
    return HCCN::Datagram (std::move (sender), std::move (buffer));
}
UDPSocket::SendStatus UDPSocket::SendDatagram (const HCCN::Datagram& datagram, std::string& error_message)
{
    const HCCN::Endpoint& endpoint = datagram.endpoint;
    const std::vector<char>& payload = datagram.payload;
    ssize_t ret = sendto (fd, payload.data (), payload.size (), MSG_DONTWAIT, (struct sockaddr*) &endpoint.address.addr, sizeof (struct sockaddr_in6));
    if (ret < 0) {
        if (errno == EAGAIN || errno == EWOULDBLOCK)
            return SendStatus::BusySocket;
        error_message = std::string ("Failed to recieve message: ") + strerror (errno);
        return SendStatus::Failure;
    }
    return SendStatus::Success;
}
