#pragma once

#include "requests.pb.h"
#include "responses.pb.h"
#include "client_to_server.h"
#include "server_to_client.h"
#include "session.h"
#include "matchstate.h"
#include "lfqueuewithnotification.h"

#include <memory>


class Room
{
public:
    struct ResponseMessage {
        RTS::Response response;
        std::shared_ptr<Session> session;
        uint64_t request_id;
    };
    struct StatsMessage {
        uint32_t player_count;
        uint32_t ready_player_count;
        uint32_t spectator_count;
    };

public:
    Room (std::shared_ptr<LFQueueWithNotification<Room::ResponseMessage>> reponse_message_queue,
          std::shared_ptr<LFQueueWithNotification<Room::StatsMessage>> stats_message_queue);
    bool start (std::string& error_message);

public:
    void receiveRequestHandlerRoom (const RTS::Request& request_oneof, std::shared_ptr<Session> session, uint64_t request_id);

public:
    void readyHandler ();
    void tick ();

public:
    std::vector<std::shared_ptr<Session>> players;
    std::shared_ptr<Session> red_team;
    std::shared_ptr<Session> blue_team;
    std::shared_ptr<MatchState> match_state;
    std::map<uint32_t, uint32_t> red_unit_id_client_to_server_map;
    std::map<uint32_t, uint32_t> blue_unit_id_client_to_server_map;
    std::vector<std::shared_ptr<Session>> spectators; // not gonna use for now
    void setError (RTS::Error* error, const std::string& error_message, RTS::ErrorCode error_code);
    void initMatchState ();
    void emitStatsUpdated ();
    int sampling = 0;
    std::optional<std::chrono::time_point<std::chrono::steady_clock>> ready_timer;
    bool match_started = false;

    std::shared_ptr<LFQueueWithNotification<Room::ResponseMessage>> reponse_message_queue;
    std::shared_ptr<LFQueueWithNotification<Room::StatsMessage>> stats_message_queue;
};
