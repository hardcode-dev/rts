template<typename Value>
LFQueue<Value>::LFQueue ()
{
    list = nullptr;
}
template<typename Value>
LFQueue<Value>::~LFQueue ()
{
    List* head = list;
    while (head) {
        List* next = head->next;
        delete head;
        head = next;
    }
}

template<typename Value>
void LFQueue<Value>::Add (Value&& value)
{
    List* new_list = new List;
    new_list->value = std::move (value);
    new_list->next = __atomic_exchange_n (&list, new_list, __ATOMIC_SEQ_CST);
}
template<typename Value>
typename LFQueue<Value>::List* LFQueue<Value>::TakeAllReversed ()
{
    return __atomic_exchange_n (&list, nullptr, __ATOMIC_SEQ_CST);
}
