#include "room_thread.h"
#include "room.h"


static void routine (RoomThread* room_thread)
{
    room_thread->run ();
}


RoomThread::RoomThread (const std::string& name)
    : name_ (name)
{
    reponse_message_queue = std::make_shared<LFQueueWithNotification<Room::ResponseMessage>> ();
    stats_message_queue = std::make_shared<LFQueueWithNotification<Room::StatsMessage>> ();
    request_message_queue = std::make_shared<LFQueueWithNotification<RequestMessage>> ();

    thread = std::unique_ptr<std::thread> (new std::thread (routine, this));
}
RoomThread::~RoomThread ()
{
    time_to_exit = true;
    thread->join ();
}
const std::string& RoomThread::name () const
{
    return name_;
}
const std::string& RoomThread::errorMessage () const
{
    return error_message;
}
uint32_t RoomThread::playerCount () const
{
    return player_count;
}
uint32_t RoomThread::readyPlayerCount () const
{
    return ready_player_count;
}
uint32_t RoomThread::spectatorCount () const
{
    return spectator_count;
}
void RoomThread::run ()
{
    Room room (reponse_message_queue, stats_message_queue);
    while (!time_to_exit) { // TODO: Implement proper termination
        {
            LFQueueWithNotification<RequestMessage>::List* message_list = request_message_queue->TakeAllReversed ();
            while (message_list) {
                RequestMessage& request_message = message_list->value;
                room.receiveRequestHandlerRoom (request_message.request_oneof, request_message.session, request_message.request_id);
                LFQueueWithNotification<RequestMessage>::List* next = message_list->next;
                delete message_list;
                message_list = next;
            }
        }
        if (room.ready_timer.has_value ()) {
            std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
            if (now >= *room.ready_timer) {
                room.readyHandler ();
                room.ready_timer = std::nullopt;
            }
        } else if (room.match_started) {
            room.tick ();
        }

        // TODO: Proper time management
        usleep (20000); // kTickDurationMs * 1000
    }
}
void RoomThread::receiveRequestHandler (const RTS::Request& request_oneof, const std::shared_ptr<Session>& session, uint64_t request_id)
{
    request_message_queue->Add (std::move (RequestMessage ({request_oneof, session, request_id})));
}
void RoomThread::updateStats (uint32_t player_count, uint32_t ready_player_count, uint32_t spectator_count)
{
    this->player_count = player_count;
    this->ready_player_count = ready_player_count;
    this->spectator_count = spectator_count;
}
