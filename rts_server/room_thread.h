#pragma once

#include "room.h"

#include "requests.pb.h"
#include "responses.pb.h"
#include "lfqueuewithnotification.h"

#include <memory>
#include <thread>


class RoomThread
{
public:
    struct RequestMessage {
        RTS::Request request_oneof;
        std::shared_ptr<Session> session;
        uint64_t request_id;
    };

public:
    RoomThread (const std::string& name);
    ~RoomThread ();
    const std::string& name () const;
    const std::string& errorMessage () const;
    uint32_t playerCount () const;
    uint32_t readyPlayerCount () const;
    uint32_t spectatorCount () const;

public:
    void run ();

public:
    void receiveRequestHandler (const RTS::Request& request_oneof, const std::shared_ptr<Session>& session, uint64_t request_id);

public:
    void updateStats (uint32_t player_count, uint32_t ready_player_count, uint32_t spectator_count);

public:
    std::unique_ptr<std::thread> thread;

    const std::string name_;
    int return_code = 0;
    std::string error_message;
    uint32_t player_count = 0;
    uint32_t ready_player_count = 0;
    uint32_t spectator_count = 0;
    std::atomic<bool> time_to_exit = false; // TODO: Use event FD

    std::shared_ptr<LFQueueWithNotification<Room::ResponseMessage>> reponse_message_queue;
    std::shared_ptr<LFQueueWithNotification<Room::StatsMessage>> stats_message_queue;

    std::shared_ptr<LFQueueWithNotification<RequestMessage>> request_message_queue;
};

