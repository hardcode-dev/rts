#pragma once


class NotificationFD
{
public:
    NotificationFD ();
    NotificationFD (const NotificationFD& notification_fd);
    NotificationFD (NotificationFD&& notification_fd) = default;
    ~NotificationFD ();
    void Notify ();
    void EnableNotifictionOnDestruction ();
    int FD ();

private:
    int fd;
    bool notify_on_destruction = false;
};
