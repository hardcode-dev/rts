#pragma once

#include "client_to_server.h"
#include "server_to_client.h"
#include "lfqueuewithnotification.h"
#include "notificationfd.h"

#include <memory>
#include <thread>


class NetworkThread
{
public:
    NetworkThread (const std::string& host, uint16_t port);
    ~NetworkThread ();
    static std::shared_ptr<std::string> Destroy (std::unique_ptr<NetworkThread>& network_thread);
    void sendDatagram (const std::shared_ptr<HCCN::ServerToClient::Message>& datagram);

public:
    NotificationFD terminate_fd;
    NotificationFD finished_fd;
    std::shared_ptr<LFQueueWithNotification<std::shared_ptr<HCCN::ClientToServer::Message>>> input_queue;
    std::shared_ptr<LFQueueWithNotification<std::shared_ptr<HCCN::ServerToClient::Message>>> output_queue;
    std::shared_ptr<std::string> error_message;
    std::unique_ptr<std::thread> thread;
};
