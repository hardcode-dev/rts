#include "notificationfd.h"

#include <sys/eventfd.h>
#include <errno.h>
#include <unistd.h>
#include <system_error>
#include <iostream>


NotificationFD::NotificationFD ()
{
    if ((fd = eventfd (0, 0)) == -1)
        throw std::system_error (errno, std::system_category (), "Failed to create event FD");
}
NotificationFD::NotificationFD (const NotificationFD& notification_fd)
{
    if ((fd = dup (notification_fd.fd)) == -1)
        throw std::system_error (errno, std::system_category (), "Failed to duplicate event FD");
}
NotificationFD::~NotificationFD ()
{
    if (notify_on_destruction)
        eventfd_write (fd, 1);
    close (fd);
}

void NotificationFD::Notify ()
{
    if (eventfd_write (fd, 1))
        throw std::system_error (errno, std::system_category (), "Failed to write to event FD");
}
void NotificationFD::EnableNotifictionOnDestruction ()
{
    notify_on_destruction = true;
}
int NotificationFD::FD ()
{
    return fd;
}
