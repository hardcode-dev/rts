#pragma once

#include "common.h"

#include <optional>
#include <vector>


namespace HCCN::ClientToServer {

struct Message {
    Message () = default;
    Message (const HCCN::Endpoint& endpoint, const std::optional<uint64_t>& session_id, uint64_t request_id, const std::vector<char>& message);
    std::vector<Datagram> encode () const;

    HCCN::Endpoint endpoint;
    std::optional<uint64_t> session_id;
    uint64_t request_id;
    std::vector<char> message;
};

struct MessageFragment {
    static std::shared_ptr<MessageFragment> parse (const HCCN::Datagram& datagram);

    HCCN::Endpoint endpoint;
    std::optional<uint64_t> session_id;
    uint64_t request_id;
    std::vector<char> fragment;
    bool is_tail;
    uint64_t fragment_number;
};

struct MessageFragmentCollector {
public:
    void insert (const std::shared_ptr<MessageFragment>& fragment);
    bool complete ();
    bool valid ();
    std::shared_ptr<Message> build ();

private:
    std::map<uint64_t, std::shared_ptr<MessageFragment>> tail;
    uint64_t max_fragment_index = 0;
    std::shared_ptr<MessageFragment> head;
};

} // namespace HCCN::ClientToServer
