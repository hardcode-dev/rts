#pragma once

#include <memory>
#include <vector>
#include <map>
#include <string_view>
#include <string.h>
#include <netinet/in.h>

namespace HCCN {

struct Endpoint {
    union {
        struct sockaddr addr;
        struct sockaddr_in addr4;
        struct sockaddr_in6 addr6;
    } address;

    bool operator== (const Endpoint& endpoint) const
    {
        if (address.addr.sa_family != endpoint.address.addr.sa_family)
            return false;
        switch (address.addr.sa_family) {
        case AF_INET:
            return
                address.addr4.sin_addr.s_addr == endpoint.address.addr4.sin_addr.s_addr &&
                address.addr4.sin_port == endpoint.address.addr4.sin_port;
        case AF_INET6:
            return
                !memcmp (address.addr6.sin6_addr.s6_addr, endpoint.address.addr6.sin6_addr.s6_addr, sizeof (address.addr6.sin6_addr.s6_addr)) &&
                address.addr6.sin6_port == endpoint.address.addr6.sin6_port;
        default:
            return false;
        }
    }
};

struct Datagram {
    Datagram (const Endpoint& endpoint, const std::vector<char>& payload)
        : endpoint (endpoint), payload (payload)
    {
    }

    Datagram (Endpoint&& endpoint, std::vector<char>&& payload)
        : endpoint (endpoint), payload (payload)
    {
    }

    Endpoint endpoint;
    std::vector<char> payload;
};

struct TransportMessageIdentifier {
    inline TransportMessageIdentifier (const Endpoint& endpoint, uint64_t message_id);
    inline bool operator== (const TransportMessageIdentifier& b) const;

    static constexpr size_t ipv6_addr_len = 16;

    uint8_t serialized[ipv6_addr_len + sizeof (uint16_t) + sizeof (uint64_t)];
};

struct TransportMessageIdentifierHash
{
    inline std::size_t operator()(const TransportMessageIdentifier& key) const noexcept;
};

} // namespace HCCN

// Implementation

inline HCCN::TransportMessageIdentifier::TransportMessageIdentifier (const Endpoint& endpoint, uint64_t message_id)
{
    if (endpoint.address.addr4.sin_family == AF_INET) {
        memcpy (serialized, &endpoint.address.addr4.sin_addr.s_addr, sizeof (uint32_t));
        memset (serialized + sizeof (uint32_t), 0, ipv6_addr_len - sizeof (uint32_t));
        memcpy (serialized + ipv6_addr_len, &endpoint.address.addr4.sin_port, sizeof (uint16_t));
    } else if (endpoint.address.addr4.sin_family == AF_INET6) {
        memcpy (serialized, endpoint.address.addr6.sin6_addr.s6_addr, ipv6_addr_len);
        memcpy (serialized + ipv6_addr_len, &endpoint.address.addr6.sin6_port, sizeof (uint16_t));
    } else {
        memset (serialized, 0, ipv6_addr_len + sizeof (uint16_t));
    }
    memcpy (serialized + ipv6_addr_len + sizeof (uint16_t), &message_id, sizeof (uint64_t));
}
inline bool HCCN::TransportMessageIdentifier::operator== (const HCCN::TransportMessageIdentifier& b) const
{
    return !memcmp (serialized, b.serialized, sizeof (serialized));
}

inline std::size_t HCCN::TransportMessageIdentifierHash::operator()(const HCCN::TransportMessageIdentifier& key) const noexcept
{
    return std::hash<std::string_view>{}({(const char*) key.serialized, sizeof (key.serialized)});
}
