find . -regex '.*\.\(cpp\|h\)' -not -path '*/\.*' -exec clang-format -style=file:.clang-format -i --dry-run -Werror {} \;
