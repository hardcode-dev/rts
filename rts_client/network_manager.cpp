#include "network_manager.h"

#include <QUdpSocket>
#include <QNetworkDatagram>


static inline QHostAddress EndpointToHostAddress (const HCCN::Endpoint& endpoint)
{
    return QHostAddress (&endpoint.address.addr);
}
static inline uint16_t EndpointGetPort (const HCCN::Endpoint& endpoint)
{
    if (endpoint.address.addr4.sin_family == AF_INET) {
        return ntohs (endpoint.address.addr4.sin_port);
    } else if (endpoint.address.addr4.sin_family == AF_INET6) {
        return ntohs (endpoint.address.addr6.sin6_port);
    } else {
        throw std::runtime_error ("Unknown network protocol");
    }
}
static inline HCCN::Endpoint HostAddressToEndpoint (const QHostAddress& host_address, uint16_t port)
{
    HCCN::Endpoint endpoint;
    memset (&endpoint.address, 0, sizeof (endpoint.address));
    if (host_address.protocol () == QAbstractSocket::IPv4Protocol) {
        endpoint.address.addr4.sin_family = AF_INET;
        endpoint.address.addr4.sin_port = htons (port);
        endpoint.address.addr4.sin_addr.s_addr = htonl (host_address.toIPv4Address ());
    } else if (host_address.protocol () == QAbstractSocket::IPv6Protocol) {
        endpoint.address.addr6.sin6_family = AF_INET6;
        endpoint.address.addr6.sin6_port = htons (port);
        Q_IPV6ADDR ipv6_addr = host_address.toIPv6Address ();
        memcpy (&endpoint.address.addr6.sin6_addr.s6_addr, &ipv6_addr, sizeof (endpoint.address.addr6.sin6_addr.s6_addr));
    } else {
        throw std::runtime_error ("Unknown network protocol");
    }
    return endpoint;
}

static inline QNetworkDatagram datagramToQtDatagramEndpointIsDestination (const HCCN::Datagram& datagram)
{
    return {QByteArray (datagram.payload.data (), datagram.payload.size ()), EndpointToHostAddress (datagram.endpoint), EndpointGetPort (datagram.endpoint)};
}
static inline HCCN::Datagram qtDatagramToDatagramEndpointIsSender (const QNetworkDatagram& network_datagram)
{
    QByteArray data = network_datagram.data ();
    return HCCN::Datagram (HostAddressToEndpoint (network_datagram.senderAddress (), network_datagram.senderPort ()), {data.data (), data.data () + data.size ()});
}


NetworkManager::NetworkManager (QObject* parent)
    : QObject (parent)
{
}

bool NetworkManager::start (QString& error_message)
{
    if (!socket.bind (QHostAddress (QHostAddress::Any), 0)) {
        error_message = socket.errorString ();
        return false;
    }
    connect (&socket, &QUdpSocket::readyRead, this, &NetworkManager::recieveDatagrams);
    connect (this, &NetworkManager::sendDatagram, this, &NetworkManager::sendDatagramHandler);
    return true;
}
std::shared_ptr<HCCN::ServerToClient::Message> NetworkManager::takeDatagram ()
{
    QMutexLocker locker (&input_queue_mutex);
    return input_queue.isEmpty () ? nullptr : input_queue.dequeue ();
}
void NetworkManager::recieveDatagrams ()
{
    while (socket.hasPendingDatagrams ()) {
        HCCN::Datagram datagram = qtDatagramToDatagramEndpointIsSender (socket.receiveDatagram ());
        if (std::shared_ptr<HCCN::ServerToClient::MessageFragment> message_fragment = HCCN::ServerToClient::MessageFragment::parse (datagram)) {
            QMutexLocker locker (&input_queue_mutex);
            HCCN::TransportMessageIdentifier transport_message_identifier (message_fragment->endpoint, message_fragment->response_id);
            std::unordered_map<HCCN::TransportMessageIdentifier, std::shared_ptr<HCCN::ServerToClient::MessageFragmentCollector>, HCCN::TransportMessageIdentifierHash>::iterator fragment_collector_it =
                input_fragment_queue.find (transport_message_identifier);
            if (fragment_collector_it != input_fragment_queue.end ()) {
                if (fragment_collector_it->second) {
                    HCCN::ServerToClient::MessageFragmentCollector& fragment_collector = *fragment_collector_it->second;
                    fragment_collector.insert (message_fragment);
                    if (fragment_collector.complete ()) {
                        input_queue.enqueue (fragment_collector.build ());
                        fragment_collector_it->second.reset ();
                    }
                }
            } else {
                std::shared_ptr<HCCN::ServerToClient::MessageFragmentCollector> fragment_collector (new HCCN::ServerToClient::MessageFragmentCollector);
                fragment_collector->insert (message_fragment);
                if (fragment_collector->complete ()) {
                    input_queue.enqueue (fragment_collector->build ());
                    fragment_collector.reset ();
                }
                input_fragment_queue.emplace (transport_message_identifier, fragment_collector);
            }
        }
    }
    emit datagramsReady ();
}
void NetworkManager::sendDatagramHandler (const HCCN::ClientToServer::Message& transport_message)
{
    std::vector<HCCN::Datagram> datagrams = transport_message.encode ();
    for (const HCCN::Datagram& datagram: datagrams)
        socket.writeDatagram (datagramToQtDatagramEndpointIsDestination (datagram));
}
