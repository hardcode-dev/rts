#pragma once

#include <QOpenGLWidget>


class QGridLayout;


class MainWindow: public QOpenGLWidget
{
    Q_OBJECT

public:
    MainWindow (QWidget* parent = nullptr);

    void setWidget (QWidget* widget);

private:
    QGridLayout* layout;
    QWidget* widget = nullptr;
};
