#pragma once

#include "matchstate.h"

class SingleModeLoader
{
public:
    static void load (std::vector<std::pair<uint32_t, Unit>>& units, std::vector<std::pair<uint32_t, Corpse>>& corpses, std::vector<std::pair<uint32_t, Missile>>& missiles);
};
